from __future__ import unicode_literals

from django.db import models

import shopify
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.db import models
from django.utils.encoding import python_2_unicode_compatible



class MyUserManager(BaseUserManager):
    def create_user(self, myshopify_domain, username=None, password=None):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not myshopify_domain:
            raise ValueError('Users must have an myshopify_domain address')
        
        if not username:
            username = myshopify_domain
            
        user = self.model(
            myshopify_domain=myshopify_domain,
            username=username,
        )
        
        if not password:
            user.set_unusable_password()
        else:
            user.set_password(password)
            
        user.save(using=self._db)
        return user

    def create_superuser(self, myshopify_domain, username, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            myshopify_domain,
            username,
            password
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    myshopify_domain = models.CharField(max_length=255, unique=True, editable=False)
    username = models.CharField(max_length=255, unique=True, editable=False)
    token = models.CharField(max_length=32, editable=False, default='00000000000000000000000000000000')
    
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = MyUserManager()

    USERNAME_FIELD = 'myshopify_domain'
    REQUIRED_FIELDS = ['username']

    @property
    def session(self):
        return shopify.Session.temp(self.myshopify_domain, self.token)

    def get_full_name(self):
        return self.myshopify_domain

    def get_short_name(self):
        return self.myshopify_domain

    def __str__(self):
        return self.get_full_name()

    
    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin