from django.apps import AppConfig


class QuickviewConfig(AppConfig):
    name = 'quickview'
