from django.shortcuts import render
from shopify_auth.decorators import login_required
import shopify

@login_required
def home(request, *args, **kwargs):
    with request.user.session:
        products = shopify.Product.find()
    return render(request, "home.html", {"products": products})